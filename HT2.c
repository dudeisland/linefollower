
/*Physical properties of vehicle:
Forward = minus-direction (MOTOR1,2)
Turning (servopos):
Maximum turn left: ~185
Maximum turn right: ~-220


Yet to be implemeted: Derivative part of controller, for line following controller. A bit jumpy still.

*/
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
#include <stdio.h>


/* OSEK declarations */
DeclareCounter(SysTimerCnt);
DeclareTask(TaskFollow);
DeclareTask(TaskDisplay);

/* Definitions */
#define MOTOR1 NXT_PORT_B
#define MOTOR2 NXT_PORT_C
#define TOUCH_SENSOR NXT_PORT_S1
#define SOUND_SENSOR NXT_PORT_S2
#define LIGHT_SENSOR NXT_PORT_S3
#define SONAR_SENSOR NXT_PORT_S4
#define MAX_STEER_LEFT 180 
#define MAX_STEER_RIGHT -180
#define MAX_LOST_INT_SEC 3
#define MAX_LOST_INT MAX_LOST_INT_SEC*20 //~No of int/sec
/* Difference of MAX_PWR defines speed */
#define MAX_PWR_FWD -90
#define MAX_PWR_BWD 85		// Default 75
#define STEER_MULTI 1;

#define PASS_KEY "1234"  /* Bluetooth pass key */
#define MAX_NUM_OF_CHAR (16)
#define MAX_NUM_OF_LINE (8)
#define MED_LIGHT 565 //The golden middle road //635 520


/*For PI functions*/
#define epsilon 1
#define dt 0.001 //10ms looptime
#define KP_FOLLOW 1		//5, 50, 1
#define KI_FOLLOW 20 //Steepens turning the longer we're off track
#define KD_FOLLOW 1 //turns us back on track

static float pre_error[2];
static float integral[2];



/* Introduction of global variables*/

U16 light;
static S16 servopos;
static S16 debug_info;
static S16 steertarget;
static U8 started = 1;

CHAR buf[BT_MAX_RX_BUF_SIZE];// BT buffer
void *ptrBuf = buf;
const char* lght = "Light: ";
const char* strsrv = "Steer pos: ";
const char* drv = "TargetSt: ";
const char* debug = "LostCnt: ";

S8 lost_flag = 0;
U16 lost_counter = 0;

/*Function definition*/
int insideWheelPower(int full, int lag);

/* nxtOSEK hook to initialize devices */
void ecrobot_device_initialize(void)
{
	
	/*Initialize BT*/
	ecrobot_init_bt_slave(PASS_KEY);
	
		
	/* Sensor initializations here */

    nxt_motor_set_speed(MOTOR1,0,0);
	nxt_motor_set_speed(MOTOR2,0,0);
    ecrobot_init_sonar_sensor(SONAR_SENSOR);
    ecrobot_set_light_sensor_active(LIGHT_SENSOR);
    
}

/* nxtOSEK hook to terminate devices */
void ecrobot_device_terminate(void)
{
    /*BT termination*/
    ecrobot_term_bt_connection();
	
    /* Insert sensor terminations here */

    nxt_motor_set_speed(MOTOR1,0,0);
    nxt_motor_set_speed(MOTOR2,0,0);
    ecrobot_term_sonar_sensor(SONAR_SENSOR);
    ecrobot_set_light_sensor_inactive(LIGHT_SENSOR);
}

/* nxtOSEK hook to realize the 1ms internal clock */
void user_1ms_isr_type2(void)
{
  StatusType ercd;

  ercd = SignalCounter(SysTimerCnt); /* Increment OSEK Alarm Counter */
  if (ercd != E_OK)
  {
    ShutdownOS(ercd);
  }
}

/*Function to find absolute value*/
float abs(float x){
	if (x<0){
		return -x;
	}else return x;
}

/* PI control algorithm, usable by steering and line following functions*/
S16 PIDcal(float setpoint, float actual_position, float Kp, float Ki, float Kd, int max, int min, int i) 
{
	U8 backupmode;
	float error;
	float derivative;
	float output;
	//Calculate P,I
	error = setpoint - actual_position;
	//In case of error too small then stop integration
	if( abs(error) > epsilon){
		integral[i] =integral[i]+ error*dt;
		
		/*Windup prevention*/
		if (((integral[i] < 0) && (error > 0)) || ((integral[i] > 0) && (error < 0))){
			integral[i] = 0;
			backupmode = 0;
		}
		if (integral[i] > MAX_LOST_INT || integral[i] < -MAX_LOST_INT){
			backupmode = 1;
		}
		
		
	}
	derivative = (error-pre_error[i])/dt;
	output = Kp*error + Ki*integral[i] + Kd*derivative;
	//Saturation Filter
	if(output> max){
		output= max;
	}
	else if(output< min){
		output= min;
	}
	//Update error
	pre_error[i] = error;
	
	//If we've been lost too long, pass special value to tell to back up
	if (backupmode){
		output = -1337;
	}
	return output;
}


/*Function to handle bt input
static void handle_input(char *data, int len)
{
	char c = data;
	// int d = *data;
	switch(c){
		case "a":
			//drive_servos = 4;
			display_string((char *)&data[0]);
			display_update();
			break;
		default:
			break;
	}

	for (i = 0; i < len; i++)
	{
		if (data[i] == '\n')
		{
			pos_x = 0;
			pos_y++;
			break;
		}
		else
		{
			display_string((char *)&data[i]);
			display_update();
			if (i == (len - 1))
			{
				pos_x += len;
				break;
			}
		}
	}
}*/

/* TaskBluetooth is executed every 5msec 
TASK(TaskBluetooth)
{
    rx_len = ecrobot_read_bt(rx_buf,0,sizeof(rx_buf));
    if (rx_len > 0)
    {
        switch(*rx_buf)
        {
        case '8': //Device will follow line
             BTcontrol = 8;
             break;
        case '5': //Device will stop movement functions
             BTcontrol = 5;
             break;
        case '1': //Calibration for background (lower) sensor value
             BTcontrol = 1;
             sensorCal(BTcontrol)
             break;
        case '2': //Calibration for line (higher) sensor value
             BTcontrol = 2;
             sensorCal(BTcontrol)
             break;
        default:
             break;
             }
    }
    TerminateTask();
}
 Function to simplify driving motors, input according to numpad numbers (8 forward..) 
void moveFunc(int dir){
	switch (dir){
		case 8://Forward is backwards ;)
			nxt_motor_set_speed(MOTOR1, MAX_PWR_FWD, 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_FWD, 0); //DEBUG BREAK
			break;
		case 7:
			nxt_motor_set_speed(MOTOR1, MAX_PWR_FWD, 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_FWD/2, 0);
			break;
		case 9:
			nxt_motor_set_speed(MOTOR1, MAX_PWR_FWD/2, 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_FWD, 0);
			steertarget = -120;
			break;
		case 2:
			nxt_motor_set_speed(MOTOR1, MAX_PWR_BWD, 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_BWD, 0);
			steertarget = 0;
			break;
		case 1:
			nxt_motor_set_speed(MOTOR1, MAX_PWR_BWD, 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_BWD/2, 0);
			steertarget = 120;
			break;
		case 3:
			nxt_motor_set_speed(MOTOR1, MAX_PWR_BWD/2, 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_BWD, 0);
			steertarget = -120;
			break;
		case 5:
			nxt_motor_set_speed(MOTOR2, 0, 1);
			nxt_motor_set_speed(MOTOR1, 0, 1);
			steertarget = 0;
			break;
		default:
			steertarget = 0; //testing
			nxt_motor_set_speed(MOTOR2, 0, 0);
			nxt_motor_set_speed(MOTOR1, 0, 0);
			break;
	}
}*/
void steerFunc(){
	if(started){
		if(steertarget == -1337){
			nxt_motor_set_speed(MOTOR1, MAX_PWR_BWD, 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_BWD, 0);
		}
		else if (steertarget < -5){
			nxt_motor_set_speed(MOTOR1, insideWheelPower(MAX_PWR_FWD, -steertarget), 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_FWD, 0);
		}else if (steertarget > 5){
			nxt_motor_set_speed(MOTOR2, insideWheelPower(MAX_PWR_FWD, steertarget), 0);
			nxt_motor_set_speed(MOTOR1, MAX_PWR_FWD, 0);
		}else{
			nxt_motor_set_speed(MOTOR1, MAX_PWR_FWD, 0);
			nxt_motor_set_speed(MOTOR2, MAX_PWR_FWD, 0);
		}
	}else{
		nxt_motor_set_speed(MOTOR1, 0, 0);
		nxt_motor_set_speed(MOTOR2, 0, 0);
	}
	
}
/*Helper function for power steering*/
int insideWheelPower(int full, int lag){
	S16 output = full + lag;
	if (output > 0) output *= STEER_MULTI;
	if (output < MAX_PWR_FWD){		// if < -85 -> == -85
		output = MAX_PWR_FWD;
	}else if (output > MAX_PWR_BWD){	// if > 85 -> == 85
		output = MAX_PWR_BWD;
	}
	return output;
}
	

/* TaskDisplay is executed every 200msec */
TASK(TaskDisplay)
{
	/*Print the light sensor status on LCD */
	display_goto_xy(0,0);
	display_string(lght);
	display_int(light, 5);
	
	/*Print the SERVO POS on LCD */
	display_goto_xy(0,1);
	display_string(strsrv);
	display_int(servopos, 5);
	
	// Print target:
	display_goto_xy(0,2);
	display_string(drv);
	display_int(steertarget, 5);

	// Print Debug:
	display_goto_xy(0,3);
	debug_info = lost_counter;
	display_string(debug);
	display_int(integral[1], 6);
	display_goto_xy(5,4);
	display_int(lost_flag, 3);
	/*Update the LCD display */
	display_update();

	TerminateTask();
}

/* Insert TaskFollow code here, used for line following.*/
TASK(TaskFollow)
{
	light = ecrobot_get_light_sensor(LIGHT_SENSOR);
	if(started){
		steertarget = PIDcal(MED_LIGHT, light, KP_FOLLOW, KI_FOLLOW, KD_FOLLOW, MAX_STEER_LEFT, MAX_STEER_RIGHT, 1);
		steerFunc();
	}

	TerminateTask();
}




TASK(TaskBackground)
{
	U32 rx_len;

	while(1)
	{
   		/* Echo back message from a PC terminal software.
   		 * E.g. Tera Term (http://ttssh2.sourceforge.jp/index.html.en)
   		 */
		rx_len = ecrobot_read_bt(buf, 0, BT_MAX_RX_BUF_SIZE);
		if (rx_len > 0)
		{
			//handle_input(ptrBuf, rx_len);
			started = !started;
			ecrobot_send_bt(buf, 0, rx_len); 
		}
	}
}
